import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { UtilsModule } from 'ngx-utils';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
	MatCardModule,
	MatToolbarModule,
	MatFormFieldModule,
	MatInputModule,
	MatSnackBarModule,
	MatListModule,
	MatIconModule,
	MatCheckboxModule
} from '@angular/material';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		UtilsModule,
		MatCardModule,
		MatToolbarModule,
		MatFormFieldModule,
		MatInputModule,
		MatSnackBarModule,
		MatListModule,
		MatIconModule,
		MatCheckboxModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
