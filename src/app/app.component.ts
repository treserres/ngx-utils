import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CustomValidators } from 'projects/ngx-utils/src/lib';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	title = 'nglib';

	form: FormGroup;

	constructor(private fb: FormBuilder) {}

	ngOnInit() {
		this.form = this.fb.group({
			cbu: ['', CustomValidators.cbu],
			cuit: ['', CustomValidators.cuit],
			importe: '',
			comprobante: '',
			numero: '',
			numform: this.fb.group({
				numero: '0045100.1235',
				borrarceros: false,
				sepmil: ',',
				sepdec: '.'
			})
		});
	}

	get numform() {
		return this.form.controls.numform as FormGroup;
	}
}
