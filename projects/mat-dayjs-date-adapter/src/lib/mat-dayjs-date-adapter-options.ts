import { MatDayjsLocaleData } from './mat-dayjs-locale-data';
import { InjectionToken } from '@angular/core';
import { MAT_DAYJS_DATE_ADAPTER_OPTIONS_FACTORY } from './mat-dayjs-date-adapter-options-factory';

/** InjectionToken for dayjs date adapter to configure options. */
export const MAT_DAYJS_DATE_ADAPTER_OPTIONS = new InjectionToken<MatDayjsDateAdapterOptions>(
	'MAT_DAYJS_DATE_ADAPTER_OPTIONS', {
		providedIn: 'root',
		factory: MAT_DAYJS_DATE_ADAPTER_OPTIONS_FACTORY
	});

export interface MatDayjsDateAdapterOptions {
	localeData: MatDayjsLocaleData;
}
