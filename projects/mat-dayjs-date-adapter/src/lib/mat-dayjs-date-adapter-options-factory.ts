import { MatDayjsDateAdapterOptions } from './mat-dayjs-date-adapter-options';
import { dayjs } from 'dayjs';

export function MAT_DAYJS_DATE_ADAPTER_OPTIONS_FACTORY(): MatDayjsDateAdapterOptions {
	return {
		localeData: {
			firstDayOfWeek: 0,
			longMonths: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
			shortMonths: 'jan_feb_mar_apr_may_jun_jul_aug_sep_oct_nov_dec'.split('_'),
			dates: range(31, (i) => dayjs().clone().set('year', 2017).set('month', 0).set('date', i + 1).format('D')),
			longDaysOfWeek: 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
			shortDaysOfWeek: 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
			narrowDaysOfWeek: 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
		}
	};
}

/** Creates an array and fills it with values. */
export function range<T>(length: number, valueFunction: (index: number) => T): T[] {
	const valuesArray = Array(length);
	for (let i = 0; i < length; i++) {
		valuesArray[i] = valueFunction(i);
	}
	return valuesArray;
}
