export interface MatDayjsLocaleData {
	firstDayOfWeek: number;
	longMonths: string[];
	shortMonths: string[];
	dates: string[];
	longDaysOfWeek: string[];
	shortDaysOfWeek: string[];
	narrowDaysOfWeek: string[];
}
