import { MatDateFormats } from '@angular/material/core';

export const DAYJS_DATE_FORMATS: MatDateFormats = {
	parse: {
		dateInput: 'MM/DD/YYYY',
	},
	display: {
		dateInput: 'MM/DD/YYYY',
		monthYearLabel: 'MMM YYYY',
		dateA11yLabel: 'MM/DD/YYYY',
		monthYearA11yLabel: 'MMMM YYYY',
	},
};
