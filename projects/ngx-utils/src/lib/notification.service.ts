import { Injectable, ElementRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class NotificationService {

	constructor(
		private snackBar: MatSnackBar
	) { }

	show(error: string, element?: ElementRef) {
		this.snackBar.open(error, 'Cerrar', {
			duration: 5 * 1000,
			panelClass: 'snack',
			verticalPosition: 'bottom',
			horizontalPosition: 'start'
		});
		if (element) {
			element.nativeElement.focus();
		}
	}
}
