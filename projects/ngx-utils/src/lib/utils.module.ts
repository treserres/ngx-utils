import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImporteDirective } from './directives/importe.directive';
import { CuitDirective } from './directives/cuit.directive';
import { ComprobanteDirective } from './directives/comprobante.directive';
import { CbuDirective } from './directives/cbu.directive';
import { NumDirective } from './directives/num.directive';
import { NumformPipe } from './pipes/numform.pipe';
import { NotificationService } from './notification.service';

@NgModule({
	declarations: [
		ImporteDirective,
		CuitDirective,
		ComprobanteDirective,
		CbuDirective,
		NumDirective,

		NumformPipe
	],
	imports: [
		CommonModule
	],
	exports: [
		ImporteDirective,
		CuitDirective,
		ComprobanteDirective,
		CbuDirective,
		NumDirective,

		NumformPipe
	],
	providers: [
		NotificationService
	]
})
export class UtilsModule { }
