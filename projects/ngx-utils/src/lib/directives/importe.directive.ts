import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
	selector: '[importeMask]'
})
export class ImporteDirective {

	constructor(
		private el: ElementRef
	) { }

	// Allow decimal numbers and negative values
	private regex: RegExp = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);

	// Allow key codes for special events. Reflect :
	// Backspace, tab, end, home
	private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight'];

	@HostListener('keydown', ['$event'])
	onKeyDown(event: any) {
		const current: string = this.el.nativeElement.value;

		// Autocompletar decimal
		if (event.key === '.') {
			if (current.length === 0) {
				event.target.value = '0.';
				event.preventDefault();
				return;
			} else if (current === '-') {
				event.target.value = '-0.';
				event.preventDefault();
				return;
			}
		}

		// No permitir otro menos que no sea al principio
		if (event.key === '-' && current.length > 0) {
			event.preventDefault();
		}

		// Allow Backspace, tab, end, and home keys
		if (this.specialKeys.indexOf(event.key) !== -1) {
			return;
		}

		// No permitir más de 2 decimales
		if (current.indexOf('.') !== -1) {
			const decimales = current.split('.')[1];
			if (decimales.length > 1) {
				event.preventDefault();
			}
		}

		const next: string = current.concat(event.key);
		if (next && !String(next).match(this.regex)) {
			event.preventDefault();
		}
	}

}
