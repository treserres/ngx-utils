import { Directive, ElementRef, HostListener, Input, Optional } from '@angular/core';
import { validarCUIT } from '../validation';
import { NotificationService } from '../notification.service';
import { NgControl } from '@angular/forms';

@Directive({
	// tslint:disable-next-line:directive-selector
	selector: '[cuitMask]'
})
export class CuitDirective {
	/** Poner en false para que sea un mask sin notificar errores (para Reactive Forms) */
	@Input() cuitMask;

	// Sólo numeros
	private numeroRegex: RegExp = new RegExp(/^[0-9]+$/);

	// Guiones opcionales
	private cuitPosibleRegex: RegExp = new RegExp(/^[0-9]{2}\-?[0-9]{8}\-?[0-9]{1}$/);

	// Guiones obligatorios
	private cuitValidoRegex: RegExp = new RegExp(/^[0-9]{2}\-[0-9]{8}\-[0-9]{1}$/);

	// Allow key codes for special events. Reflect :
	// Backspace, tab, end, home
	private specialKeys: Array<string> = ['Delete', 'Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight'];
	cancel = false;

	constructor(private el: ElementRef, private notification: NotificationService, @Optional() private ngControl: NgControl) { }

	@HostListener('paste', ['$event'])
	onPaste(event: any) {
		const value = event.target.value;
		if (typeof value === 'string') {
			const paste: string = event.clipboardData.getData('Text').trim();
			const seleccionado = {
				start: event.target.selectionStart,
				length: event.target.selectionEnd - event.target.selectionStart
			};
			if (value === paste && seleccionado.length > 0) {
				event.preventDefault();
				event.target.selectionStart = value.length;
				event.target.selectionEnd = value.length;
			} else if (seleccionado.length > 0 && this.cuitPosibleRegex.test(paste)) {
				if (value.length <= 13) {
					const resto = value.substring(seleccionado.start + seleccionado.length, value.length);
					// tslint:disable-next-line: max-line-length
					this.setValue(
						value.substring(0, seleccionado.start) +
						paste.substring(0, 13 - value.length) +
						resto.substring(0, 13 - (value.substring(0, seleccionado.start) + paste.substring(0, 13 - value.length)).length)
					);
				} else {
					this.setValue(
						value.substring(0, seleccionado.start) +
						paste.substring(0, seleccionado.length) +
						value.substring(seleccionado.start + seleccionado.length, value.length)
					);
				}
				event.preventDefault();
			} else if (value.length + paste.length > 13 && this.cuitPosibleRegex.test(paste)) {
				this.setValue(value + paste.substring(0, 13 - value.length));
				event.preventDefault();
			} else if (!this.cuitPosibleRegex.test(paste)) {
				if (this.cuitValidoRegex.test(paste)) {
					this.setValue(paste);
				} else if (this.numeroRegex.test(paste)) {
					this.setValue(value + paste.substring(0, 13 - value.length));
				}
				event.preventDefault();
			} else {
				this.setValue(value + paste);
			}
		}
	}

	@HostListener('keydown', ['$event'])
	onKeyDown(event: any) {
		if (this.specialKeys.indexOf(event.key) !== -1 || event.ctrlKey) {
			return;
		}

		const current = this.el.nativeElement.value;
		const next: string = current.concat(event.key);

		/** Si no es tecla especial o número */
		if (!this.numeroRegex.test(event.key) && event.key !== 'Enter' && event.key !== '-') {
			event.preventDefault();
			return;
		}

		/** Si está seleccionado, reemplazo */
		const seleccionado =
			this.el.nativeElement.selectionStart === 0 && this.el.nativeElement.selectionEnd === current.length && current.length > 0;
		if (seleccionado && this.numeroRegex.test(event.key)) {
			this.setValue(event.key);
			event.preventDefault();
			return;
		}

		if (next.length > 13 && event.key !== 'Enter') {
			event.preventDefault();
			return;
		}

		if (this.numeroRegex.test(event.key)) {
			if ((current.length === 2 || current.length === 11) && (current.match(/-/g) || []).length < 2) {
				this.setValue(current + '-' + event.key);
			} else if ((next.length === 2 || next.length === 11) && (next.match(/-/g) || []).length < 2) {
				this.setValue(current + event.key + '-');
			} else {
				if (
					event.target.selectionEnd !== 11 &&
					event.target.selectionEnd !== 2 &&
					event.target.selectionEnd === event.target.selectionStart
				) {
					return;
				}
				this.setValue(current + event.key);
			}
		} else if (event.key === 'Enter') {
			const res = validarCUIT(event.target.value);
			if (typeof res === 'string') {
				if (this.cuitMask.length > 0) {
					this.notification.show(res);
				}
			}
		}
		// this.setValue(event.target.value);
		event.preventDefault();
	}

	@HostListener('blur', [])
	onBlur() {
		const res = validarCUIT(this.el.nativeElement.value);
		if (typeof res === 'string') {
			if (this.cuitMask.length > 0) {
				this.notification.show(res);
			}
		}
	}

	private setValue(value: any) {
		setTimeout(() => {
			if (value) {
				if (value.substring(2, 3) && value.substring(2, 3) !== '-') {
					value = value.substring(0, 2) + '-' + value.substring(2, 11);
				}
				if (value.substring(11, 12) && value.substring(11, 12) !== '-') {
					value = value.substring(0, 11) + '-' + value.substring(11, 12);
				}
			}
			if (this.ngControl) {
				this.ngControl.control.setValue(value);
			} else {
				this.el.nativeElement.value = value;
			}
		});
	}
}
