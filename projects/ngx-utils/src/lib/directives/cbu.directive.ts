import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { validarCBU } from '../validation';
import { NotificationService } from '../notification.service';
import { NgControl } from '@angular/forms';

@Directive({
	// tslint:disable-next-line:directive-selector
	selector: '[cbuMask]'
})
export class CbuDirective {
	// Allow decimal numbers
	private regex: RegExp = new RegExp(/^[0-9]+$/);

	// Allow key codes for special events. Reflect :
	// Backspace, tab, end, home
	private specialKeys: Array<string> = ['Delete', 'Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight'];

	@Input() cbuMask;

	constructor(private el: ElementRef, private notification: NotificationService, private ngControl: NgControl) { }

	@HostListener('drop', ['$event'])
	onDrop(event) {
		event.preventDefault();
	}

	@HostListener('paste', ['$event'])
	onPaste(event: any) {
		this.parseValue(event.clipboardData.getData('Text'));
	}

	parseValue(data) {
		const value = this.el.nativeElement.value;
		if (typeof value === 'string') {
			const seleccionado = {
				start: this.el.nativeElement.selectionStart,
				length: this.el.nativeElement.selectionEnd - this.el.nativeElement.selectionStart
			};
			if (value === data && seleccionado.length > 0) {
				event.preventDefault();
				this.el.nativeElement.selectionStart = value.length;
				this.el.nativeElement.selectionEnd = value.length;
			} else if (seleccionado.length > 0 && this.regex.test(data)) {
				setTimeout(() => {
					if (value.length < 22) {
						const resto = value.substring(seleccionado.start + seleccionado.length, value.length);
						// tslint:disable-next-line: max-line-length
						this.setValue(
							value.substring(0, seleccionado.start) +
							data.substring(0, 22 - value.length) +
							resto.substring(
								0,
								22 - (value.substring(0, seleccionado.start) + data.substring(0, 22 - value.length)).length
							)
						);
					} else {
						this.setValue(
							value.substring(0, seleccionado.start) +
							data.substring(0, seleccionado.length) +
							value.substring(seleccionado.start + seleccionado.length, value.length)
						);
					}
				});
				event.preventDefault();
				return;
			} else if (value.length + data.length > 22 && this.regex.test(data)) {
				setTimeout(() => {
					this.setValue(value + data.substring(0, 22 - value.length));
				});
				event.preventDefault();
			} else if (!this.regex.test(data)) {
				event.preventDefault();
			}
		}
	}

	@HostListener('keydown', ['$event'])
	onKeyDown(event: any) {
		if (this.specialKeys.indexOf(event.key) !== -1) {
			return;
		}

		if (event.ctrlKey) {
			return;
		}

		const esNumero = this.regex.test(event.key);

		/** Si no es tecla especial o número */
		if (!esNumero && event.key !== 'Enter') {
			event.preventDefault();
			return;
		}

		const current = this.el.nativeElement.value;
		const next: string = this.el.nativeElement.value.concat(event.key);

		/** Si está seleccionado, reemplazo */
		const seleccionado =
			this.el.nativeElement.selectionStart === 0 && this.el.nativeElement.selectionEnd === current.length && current.length > 0;
		if (seleccionado && esNumero) {
			event.preventDefault();
			this.setValue(event.key);
			return;
		}

		if (next.length > 22 && event.key !== 'Enter') {
			event.preventDefault();
			return;
		}

		if (esNumero) {
			this.setValue(
				current.substring(0, event.target.selectionStart) + event.key + current.substring(event.target.selectionEnd, next.length),
				(current.substring(0, event.target.selectionStart) + event.key).length
			);
		} else if (event.key === 'Enter') {
			const res = validarCBU(event.target.value);
			if (typeof res === 'string') {
				this.notification.show(res, this.el);
			}
		}
		event.preventDefault();
	}

	@HostListener('blur', [])
	onBlur() {
		const res = validarCBU(this.el.nativeElement.value);
		if (this.cbuMask.length !== 0 && typeof res === 'string') {
			this.notification.show(res, this.el);
		}
	}

	private setValue(value: any, selectionIndex: number = null) {
		setTimeout(() => {
			if (this.ngControl) {
				this.ngControl.control.setValue(value);
			} else {
				this.el.nativeElement.value = value;
			}
			if (selectionIndex !== null) {
				this.el.nativeElement.selectionStart = selectionIndex;
				this.el.nativeElement.selectionEnd = selectionIndex;
			}
		});
	}
}
