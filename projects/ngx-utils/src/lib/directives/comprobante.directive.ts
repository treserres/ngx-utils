import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
	selector: '[comprobanteMask]'
})
export class ComprobanteDirective {

	constructor(
		private el: ElementRef,
	) { }

	// Allow decimal numbers and negative values
	private regex: RegExp = new RegExp(/^[0-9]$/g);

	// Allow key codes for special events. Reflect :
	// Backspace, tab, end, home
	private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight'];

	@HostListener('keydown', ['$event'])
	onKeyDown(event: any) {
		if (this.specialKeys.indexOf(event.key) !== -1) {
			return;
		}

		const esNumero = String(event.key).match(this.regex);

		/** Si no es tecla especial o número */
		if (!esNumero && event.key !== 'Enter') {
			event.preventDefault();
			return;
		}

		const current = this.el.nativeElement.value;
		const next: string = this.el.nativeElement.value.concat(event.key);

		/** Si está seleccionado, reemplazo */
		const seleccionado = this.el.nativeElement.selectionStart === 0 && this.el.nativeElement.selectionEnd === current.length && current.length > 0;
		if (seleccionado && esNumero) {
			event.target.value = event.key;
			event.preventDefault();
			return;
		}

		if (next.length > 13 && event.key !== 'Enter') {
			event.preventDefault();
			return;
		}

		if (event.key === 'Enter') {
			const guion = current.indexOf('-');
			if (guion === -1) {
				event.target.value = '0000-' + '0'.repeat(8 - current.length) + current;
			} else {
				const valor = current.replace('-', '');
				event.target.value = '0'.repeat(4 - valor.substring(0, guion).length) + valor.substring(0, guion) + '-' + '0'.repeat(8 - valor.substring(guion, valor.length).length) + valor.substring(guion, valor.length);
			}
		} else if (current.length === 4 && !current.includes('-')) {
			event.target.value = current + '-' + event.key;
			event.preventDefault();
		}
	}

}
