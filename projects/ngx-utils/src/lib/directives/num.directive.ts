import { Directive, ElementRef, HostListener, Input, Host, Self, Optional } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
	// tslint:disable-next-line:directive-selector
	selector: '[numMask]'
})
export class NumDirective {
	// Allow decimal numbers and negative values
	private regex: RegExp = new RegExp(/^-?[0-9]+$/);

	// Allow key codes for special events. Reflect :
	// Backspace, tab, end, home
	private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight'];

	@Input('numMask') numMask: string;

	constructor(
		private el: ElementRef,
		@Optional() private ngControl: NgControl
	) { }

	@HostListener('paste', ['$event'])
	onPaste(event: any) {
		const value = event.target.value;
		if (typeof value === 'string') {
			const paste: string = event.clipboardData.getData('Text');
			const seleccionado = {
				start: event.target.selectionStart,
				length: event.target.selectionEnd - event.target.selectionStart
			};
			if (value === paste && seleccionado.length > 0) {
				event.preventDefault();
				event.target.selectionStart = value.length;
				event.target.selectionEnd = value.length;
			} else if (this.numMask.length > 0 && value.length + paste.length > Number.parseInt(this.numMask, 10)) {
				this.setValue(value + paste.substring(0, 13 - value.length));
				event.preventDefault();
			} else if (!this.regex.test(paste)) {
				event.preventDefault();
			} else {
				this.setValue(value + paste);
			}
		}
	}

	@HostListener('keydown', ['$event'])
	onKeyDown(event: KeyboardEvent) {

		const current: string = this.el.nativeElement.value;

		if (event.ctrlKey) {
			return;
		}

		// Allow Backspace, tab, end, and home keys
		if (this.specialKeys.indexOf(event.key) !== -1) {
			return;
		}

		if (this.numMask.length > 0 && current.length === Number.parseInt(this.numMask, 10)) {
			event.preventDefault();
		}

		const next: string = current.concat(event.key);
		if (next && !this.regex.test(String(next))) {
			event.preventDefault();
		}
	}

	private setValue(value: any) {
		setTimeout(() => {
			if (this.ngControl) {
				this.ngControl.control.setValue(value);
			} else {
				this.el.nativeElement.value = value;
			}
		});
	}
}
