import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'numform'
})
export class NumformPipe implements PipeTransform {

	transform(
		valor: any,
		borrarceros: boolean = false,
		sepmil: string = ',',
		sepdec: string = '.'
	): any {
		let encero = '';
		if (!borrarceros) { encero = '0.00'; }
		if (!valor || isNaN(valor)) { valor = 0; }
		valor = valor.toString().replace(/\$|\,/g, '');
		if (valor === 0) { return encero; }
		const sign = (Number.parseFloat(valor) === Math.abs(Number.parseFloat(valor)));
		valor = Math.abs(Number.parseFloat(valor));
		valor = Math.floor((Number.parseFloat(valor) * 100) + 0.50000000001);
		let cents: any = valor % 100;
		if (cents < 10) {
			cents = '0' + cents;
		}
		valor = Math.floor(valor / 100).toString();
		for (let i = 0; i < Math.floor((valor.length - (1 + i)) / 3); i++) {
			valor = valor.substring(0, valor.length - (4 * i + 3)) + sepmil + valor.substring(valor.length - (4 * i + 3));
		}
		return (((sign) ? '' : '-') + valor + sepdec + cents);
	}

}
